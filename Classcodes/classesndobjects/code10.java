class Demo { 

	 static { 
		 System.out.println ( "Static block" ) ; 
	 }
	 public static void main ( String [] ar ) { 
		 System.out.println ( "In Main Demo" ) ; 
	 } 

}
class Client { 
	static { 
		System.out.println ( "Static block 2" ) ; 
	}
	public static void  main ( String  [] ar ) { 
		System.out.println ( "In client main" ) ; 
	}
	static { 
		System.out.println ( "Static block 3 " ) ; 
	}
}
