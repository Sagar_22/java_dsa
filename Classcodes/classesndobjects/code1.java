class ObjectDemo {
                ObjectDemo() {
                        System.out.println("Inside Constructor");
                }
                void fun(){
                        ObjectDemo obj = new ObjectDemo();
                        System.out.println(obj);
                }
                public static void main(String args []) {
                        ObjectDemo obj1 = new ObjectDemo();
                        System.out.println(obj1);
                        ObjectDemo obj2 = new ObjectDemo();
                        System.out.println(obj2);
                        obj1.fun();
                }
}
