class  InstanceDemo { 
	int x  = 10;
        InstanceDemo () { 
		System.out.println ( "Constructor" ) ; 
	}
	{
		System.out.println ( "Instance block 1" ) ;
	}
	public static void main ( String [] ar ) {
	InstanceDemo obj = new InstanceDemo () ; 
		System.out.println ( "In main ") ;  
	}
		{
			System.out.println ( "Instance block 2" ) ; 
		}
}

