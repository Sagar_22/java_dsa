//Jagged array initialization second method

import java.io.*;
class Jagged3{

        public static void main (String ar[])throws IOException{

		// int arr [][] = {{10,20,30},{40,50},{60}};
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
	
			int arr1 [][] = new int [3][];
               
		System.out.println("enter size arr[0] = ");
		int a = Integer.parseInt(br.readLine());

		System.out.println("enter size arr[1] = ");
		int b = Integer.parseInt(br.readLine());

		System.out.println("enter size arr[2] = ");
		int c = Integer.parseInt(br.readLine());


		 arr1[0] = new int [a];
	         arr1[1] = new int [b];
	         arr1[2] = new int [c];
		

                for(int i = 0; i < arr1.length; i++){
			System.out.println("enter arr1["+i+"] array");
                        for(int j = 0; j < arr1[i].length; j++){

                                arr1[i][j] = Integer.parseInt(br.readLine());
                        }
                        
                }

		 for(int [] x : arr1){
		 
		 	for(int y : x){
				System.out.print(y+" ");
			}
			System.out.println();
		 }
        }
}
