//que2.given an integer n
//print sum of its digit
//assume: n >= 0
//
//input: 6531
//output: 15

import java.util.*;
class Sum {
	public static void main (String args[]){
		Scanner sc = new Scanner (System.in);
	       			int n = sc.nextInt();
					int sum=0;
				while(n!=0){
					int i = n%10;
					sum = sum + i;
					n =n/10;		
							}
				System.out.println(sum);
				
			}
			 
	}

