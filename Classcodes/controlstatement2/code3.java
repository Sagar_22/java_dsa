//que3.take an integer n as input
//print multiplication of its digit
//
//input:6531
//output:90

import java.util.*;
class Multi {
	public static void main (String args []){
		Scanner sc = new Scanner (System.in);
		  int n = sc.nextInt();
		  int mul = 1;
		  while(n!=0){
		  	int i = n%10;
			mul =mul *i;
			n = n/10;

		  }
		  System.out.println(mul);
	}

}
