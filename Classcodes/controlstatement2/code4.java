//Que4.take an integer n has input 
//print perfect square till n
//
//25 --> 5 :yes
//81 --> 9 :yes
//1 --> 1 :yes
//10 --> 3.13 :no
//
//input: 30
//output: 1 4 9 16 25


//POSSIBILITY:1

import java.util.*;
class Square{
	public static void main (String args[]){
			Scanner sc = new Scanner (System.in);

			int n = sc.nextInt();
				int i = 1;
			while(i*i<=n){
			System.out.println(i*i);
						i++;
			}
	}
}

//possibility :2 :-loop travel extra time.
/*
import java.util.*;
class Square{
        public static void main (String args[]){
                        Scanner sc = new Scanner (System.in);
				int n = sc.nextInt();
				int i = 1;
			while(i<=n){
				if (i*i<=n){
					System.out.println(i*i);
				}	
				i++;
			}
                                
                       }
        }


*/


