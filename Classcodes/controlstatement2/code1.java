//que1.give an integer n
//	print all its digit
//input: 6531
//output:1
//	 3
//	 5
//	 6

import java.util.*;
class Print{
 	public static void main(String args []){
		Scanner sc = new Scanner (System.in);
			int n = sc.nextInt();
		while(n!=0){
			int i = n%10;
			System.out.println(i);
			n=n/10;
		}
	}
}

