//electricity bill problem
//given an integer input A which represent unit of electricity consumed at per house 
//calculate & print the bill amount 
//unit <= 100    :price per unit is 1
//unit >  100    :price per unit is 2
//input1: 50
//output: 50
//
//input2: 200
//output: 300

import java.util.*;

class Bill {
 	public static void main(String args[]){
        	Scanner sc = new Scanner (System.in);

		         int unit;
		System.out.print("enter the unit :- ");
	             unit = sc.nextInt();
			
		 if (unit<100) {
		 	System.out.println(unit*1);
		 }   	
		 else {
		 	 System.out.println((unit-100)*2+100);
		 }
	}
}

