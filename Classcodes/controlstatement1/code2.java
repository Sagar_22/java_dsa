//QUE2. take two integer a & b as input
//print the max of two
//assume :  a and b are not same
//		x = 10 y = 20
//output : 20 is greater

class Max{
	public static void main(String args[]){
		int x = 10;
		int y = 20;

	     if (x>y){
	     	System.out.println(x+" is greater");
	     }	

	     else{
	     	 System.out.println(y+" is greater");
	     }
	}
}
