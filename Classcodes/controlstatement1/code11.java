//QUE11 Take an integer as input print odd integer from 1 to n using while loop
//input: 10
//output: 1 3 5 7 9

import java.util.*;
class Odd_print{
	public static void main (String args[]){
		Scanner sc = new Scanner (System.in);
		int n;
		System.out.print("enter the value of n :- ");
		n = sc.nextInt();
		int i = 0;
		while(i<=n){
			if (i%2==1){
				System.out.println(i);
					
			}
				i++;
		}
	}
}
