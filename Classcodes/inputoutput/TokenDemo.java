//student name,grade,marks,CGPA.

import java.util.*;
import java.io.*;

class TokenDemo {
        public static void main (String ar[])throws IOException {
                BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
                System.out.println("enter student details:- Name,Grade,Marks,CGPA.");
                String info = br.readLine();
                System.out.println(info);

                StringTokenizer st= new StringTokenizer(info," ");

                String name = st.nextToken();
                char grade = st.nextToken().charAt(0);
                int marks = Integer.parseInt(st.nextToken());
                float cgpa = Float.parseFloat(st.nextToken());

                System.out.println("name = "+name);
                System.out.println("grade = "+grade);
                System.out.println("marks = "+marks);
                System.out.println("cgpa = "+cgpa);

        }
}