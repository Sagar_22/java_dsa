/*Program 1
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
*/
import java.io.*;
class ElementDigitReverse {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enetr Total number of Elements :");
                int num = Integer.parseInt(br.readLine());
                int arr[] = new int[num];
                System.out.println("Enetr Elements :");
                for(int i=0; i<arr.length; i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }
                for(int i=0; i<arr.length; i++) {
                        int x=arr[i];
                        int rev =0;
                        while(x!=0) {
                                int rem=x%10;
                                rev = rev*10+rem;
                                x=x/10;
                        }
                        System.out.print(rev+" ");
                }
        }
}