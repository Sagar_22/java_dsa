/*
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2
*/

import java.io.*;
class SearchElement {
        public static void main (String ar[])throws IOException {
                BufferedReader br= new BufferedReader (new InputStreamReader (System.in));
                System.out.println("Enter number of elements");
                int size= Integer.parseInt(br.readLine());
                int arr[] =new int[size];
                System.out.println("Enter array elements");
                for(int i=0; i<size; i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                System.out.println("Enter element to search = ");
                int ele = Integer.parseInt(br.readLine());
                System.out.println(arr[ele]);

        }
}