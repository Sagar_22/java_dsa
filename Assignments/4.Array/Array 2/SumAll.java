/*
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and find the sum of all elements in the array.
*/

import java.io.*;
class SumAll {
        public static void main (String ar[])throws IOException {
                BufferedReader br= new BufferedReader (new InputStreamReader (System.in));
                System.out.println("Enter number of elements");
                int size= Integer.parseInt(br.readLine());
                int arr[] =new int[size];
                int sum = 0;
                System.out.println("Enter array elements");
                for(int i=0; i<size; i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                        sum = sum + arr[i];
                }
                System.out.println("Sum of all elements = "+sum);
        }
}