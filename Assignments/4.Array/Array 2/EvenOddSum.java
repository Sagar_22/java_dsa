/*

   Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26

*/

import java.io.*;
class EvenOddSum {
        public static void main (String ar[])throws IOException {
                BufferedReader br= new BufferedReader (new InputStreamReader (System.in));
                System.out.println("Enter number of elements");
                int size= Integer.parseInt(br.readLine());
                int arr[] =new int[size];
                int oddSum = 0;
                int evenSum = 0;
                System.out.println("Enter array elements");
                for(int i=0; i<size; i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                        if(arr[i]%2==0) {
                                evenSum= evenSum+arr[i];
                        }
                        else{
                                oddSum= oddSum+arr[i];
                        }
                }
                System.out.println("Sum of Even Elements: "+evenSum);
                System.out.println("Sum of Odd Elements : "+oddSum);
        }
}