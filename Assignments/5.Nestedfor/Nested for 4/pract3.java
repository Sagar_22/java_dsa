/*
10
10 9
9 8 7
7 6 5 4
*/
class C2W {
        public static void main (String ar[]) {
                int row = 4;
                int num = 10;
                for (int i=1; i<=row; i++) {
                        for(int j=1; j<=i; j++) {
                                if(j==i) {
                                        System.out.print(num+ " ");

                                }
                                else {
                                        System.out.print(num-- +" ");
                                }
                        }
                        System.out.println();
                }
        }
}