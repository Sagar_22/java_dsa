/*write a program to print a series of strong numbers from entered range. ( Take a start and end number
from a user )
Input:-
Enter starting number: 1
Enter ending number: 150
Output:-
Output: strong numbers between 1 and 150
1 2 145
*/
import java.io.*;
class StrongNo {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter start limit");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter end limit");
                int end =  Integer.parseInt(br.readLine());

                for(int i=start; i<=end; i++){
                        int num=i;
                        int add = 0;
                        while(num!=0) {
                                int rem = num%10;
                                int mult = 1;
                                for(int j=rem; j>0; j--) {
                                        mult = mult*j;
                                }
                                add= add+mult;
                                num = num/10;
                        }
                        if(add==i) {
                                System.out.print(i+" ");
                        }
                }
        }
}
