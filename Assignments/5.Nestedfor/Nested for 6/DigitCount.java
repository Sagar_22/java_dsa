/*
Write a program to take 5 numbers as input from the user and print the count of digits in those
numbers.
Input: Enter 5 numbers :
5
The digit count in 5 is 1
25
The digit count in 25 is 2
225
The digit count in 225 is 3
6548
The digit count in 6548 is 4
852347
The digit count in 852347 is 6
*/
import java.io.*;
class DigitCount {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                for(int i =1; i<=5; i++) {
                        System.out.println("Enter the number");
                        int num =  Integer.parseInt(br.readLine());
                        int number = num;
                        int count = 0;
                        while(num!=0) {
                                count++;
                                num = num/10;
                        }
                        System.out.println("The Digit Count of number "+number+" is "+count);
                }
        }
}
