import java.io.*;
class PerfectSquare {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter start limit");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter end limit");
                int end =  Integer.parseInt(br.readLine());
                for(int i=start; i<=end; i++) {
                        if((i*i)<=end) {
                                System.out.print((i*i)+" ");
                        }
                }
        }
}