//que4.count odd digit of the given number
//input: 942111423
//output: 5


import java.util.*;
class CountOdd{
        public static void main(String bp[]){
                        Scanner sc = new Scanner (System.in);
                                int n = sc.nextInt();
                                        int count = 0;
                                while(n!=0){
                                        int i = n%10;
                                        if(i%2==1)
						count++;
                                        n = n/10;
                                }
                                 System.out.println(count);
        }
}
