//que5.print square of even digits
//input: 942111423
//output: 4 16 4 16


import java.util.*;
class Square{
public static void main(String args[]){
	Scanner sc = new Scanner(System.in);

	int n = sc.nextInt();

	while(n!=0){
		int v = n%10;
		if (v%2==0){
			System.out.println(v*v);
		}
			n = n/10;
	}
	
}

}
