//que6.write a program to print 
//the sum of all even number and 
//multiplication  of odd number between 1 to 10


class Print{
	public static void main(String args[]){
		
		int i = 1;
		int sum = 0;
		int mul = 1;

		while(i<=10){
			if(i%2==0){
				sum = sum+i;
			}
			else{
				mul = mul*i;
			}
				i++;	
		}

		System.out.println("sum of even number from 1 to 10 is "+sum);
		System.out.println("multiplication of even number from 1 to 10 is "+mul);
	}
}
